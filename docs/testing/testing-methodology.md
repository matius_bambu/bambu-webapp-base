# Testing Methodology

In order to give confidence that the application that we're building works as expected, testing is necessary.

There are a few types of testing:

1. Unit test - test individual, isolated component.
2. Integration test - test that multiple components work exactly like how they're intended to be.
3. End-to-end test - test the app, end-to-end using a helper "robot".

For our application development, we will only use integration test because it gives us the perfect balance between the confidence and speed/time consumed.

## Integration test

Thing to remember is [**Write tests. Not too many. Mostly integration.**](https://kentcdodds.com/blog/write-tests).

For integration test, our main focus is to test a feature based on how the user is going to use that feature.

For example, we have a login page where:

- The user is asked to put their username and password into corresponding fields.
- The user can submit those data that they just fill in, which ultimately will trigger an API call to authenticate the user's credentials.
- If the user's credentials is verified to be correct, then user will be notified that they have successfully logged into the app.

In order to be able to test those scenarios above, we are going to use [**@testing-library/react**](https://testing-library.com/docs/react-testing-library/intro/).

```
import userEvent from '@testing-library/user-event';

import { render, screen } from 'utils/testUtils';

import { ExamplePage } from '..';

describe('ExamplePage', () => {
    describe('successful submission', () => {
        test('display message if user is successfully logged in', async () => {
            render(<ExamplePage />);

            // type username
            const userNameField = screen.getByLabelText(/username/i);
            userEvent.type(userNameField, 'matius@bambu.co');

            // type password
            const passwordField = screen.getByLabelText(/password/i);
            userEvent.type(passwordField, 'Bambu@01');

            // submit form
            const submitBtn = await screen.findByRole('button', { name: /submit/i });
            userEvent.click(submitBtn);

            // message is displayed
            const loginMessage = await screen.findByText(/you have successfully logged in!/i);
            expect(loginMessage).toBeInTheDocument();
        });
    });
});
```

In short, what happens in those lines of code above is:

1. We target username field, and type in the username.
2. We target password field, and type in the password.
3. We target the button, and we click the button (which will trigger mock server /login API).
4. Message will be displayed if it's successful.
