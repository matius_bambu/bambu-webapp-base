# Commit Hooks

There are 2 commit hooks that we integrate into this codebase:

- pre-commit
- pre-push

## pre-commit hook

Pre-commit hook is run a moment before a commit is made (`git commit -m "commit message here"`).
During this stage, we are going to run eslint to catch possible severe linting errors and prettier to prettify the line of codes.

## pre-push hook

Pre-push hook is run before the code is pushed to remote repository (`git push origin`).
At this stage, we are going to run the project test to ensure that we pass all tests before the code is pushed.
