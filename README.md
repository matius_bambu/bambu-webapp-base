# BAMBU-webapp-base (Typescript)

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

### Overall Directory Structure

At a high level, the structure looks roughly like this:

```
webapp/
  |- .husky/
  |- .storybook/
  |  |- <Storybook configuration>
  |- docs/
  |  |- <Important documentations about repo tech stack/FAQ>
  |- public
  |- src/
  |  |- api/
  |  |  |- <API config & APIs>
  |  |- App/
  |  |- components/
  |  |  |- <shared components, i.e UI, Form, Icons>
  |  |- hooks/
  |  |  |- <reusable hooks>
  |  |- pages/
  |  |  |- <application pages>
  |  |- stories/
  |  |  |- <storybook examples>
  |  |- types/
  |  |  |- <shared types>
  |  |- utils/
  |  |  |- <utilities function>
  |  |- configureStore.ts
  |  |- createTheme.ts
  |  |- index.tsx
  |  |- reducers.ts
  |  |- reportWebVitals.ts
  |  |- sagas.ts
  |  |- setupTests.jts
  |- .env.{ENVIRONMENT}
  |- .eslintignore
  |- .eslintrc.js
  |- .gitignore
  |- .prettierignore
  |- .prettierrc
  |- lint-staged.config.js
  |- package.json
  |- tsconfig.json
  |- README.md
  |- yarn.lock
```

### Important Documentation

- [**Commit Hooks**](docs/commit-hooks.md)
- [**Testing Methodology**](docs/testing/testing-methodology.md)
