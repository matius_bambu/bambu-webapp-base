/// <reference types="cypress" />

describe('bambu-webapp-base', () => {
  beforeEach(() => {
    cy.visit('http://localhost:3000');
  });

  it('successful login with correct credentials', () => {
    cy.findByRole('textbox', { name: /username/i }).type('matius@bambu.co');
    cy.findByLabelText(/password/i).type('Bambu@01');
    cy.findByRole('button', { name: /submit/i }).click();

    cy.findByText(/you are logged in/i);
  });
});
