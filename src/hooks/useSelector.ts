import { TypedUseSelectorHook, useSelector as useAppSelector } from 'react-redux';

import type { RootState } from 'configureStore';

export const useSelector: TypedUseSelectorHook<RootState> = useAppSelector;
