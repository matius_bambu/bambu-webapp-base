import { ThemeProvider } from '@material-ui/core/styles';
import { render } from '@testing-library/react';
import { HelmetProvider } from 'react-helmet-async';
import { Provider } from 'react-redux';
import { MemoryRouter } from 'react-router-dom';

import { store } from 'configureStore';
import { theme } from 'createTheme';

import type { ReactElement, ComponentType } from 'react';

export type AllTheProvidersProps = {
  children: ReactElement;
};
const AllTheProviders = ({ children }: AllTheProvidersProps) => (
  <Provider store={store}>
    <MemoryRouter>
      <ThemeProvider theme={theme}>
        <HelmetProvider>{children}</HelmetProvider>
      </ThemeProvider>
    </MemoryRouter>
  </Provider>
);

const customRender = (
  ui: ReactElement,
  options?: {
    route?: string;
  },
) => {
  if (options && options.route) {
    window.history.pushState({}, 'Test page', options.route);
  }

  return render(ui, { wrapper: AllTheProviders as ComponentType, ...options });
};

// re-export everything
export * from '@testing-library/react';

// override render method
export { customRender as render };
