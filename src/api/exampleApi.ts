import axios, { AxiosPromise } from 'axios';

export type SubmitCredentialRequestProps = {
  userName: string;
  password: string;
};

export const submitCredential = ({
  userName,
  password,
}: SubmitCredentialRequestProps): AxiosPromise<{ success: boolean }> =>
  axios.post('/login', { userName, password });
