import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';

import type { ReactNode } from 'react';

export type FieldWrapperProps = {
  children: ReactNode;
  errorMessage?: string;
};

export const FieldWrapper = ({ children, errorMessage }: FieldWrapperProps): JSX.Element => (
  <Grid container>
    <Grid item xs={12}>
      {children}
    </Grid>
    {errorMessage ? (
      <Grid item xs={12}>
        <Typography color="error" variant="caption">
          {errorMessage}
        </Typography>
      </Grid>
    ) : null}
  </Grid>
);
