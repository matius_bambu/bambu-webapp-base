import { configureStore, MiddlewareArray } from '@reduxjs/toolkit';
import { routerMiddleware } from 'connected-react-router';
import { createBrowserHistory } from 'history';
import createSagaMiddleware from 'redux-saga';

import { createReducer } from './reducers';
import { rootSaga } from './sagas';

const configureAppStore = (preloadedState: Record<string, unknown> = {}) => {
  const history = createBrowserHistory();
  const sagaMiddleware = createSagaMiddleware();
  const store = configureStore({
    devTools: process.env.NODE_ENV !== 'production',
    reducer: createReducer(history),
    middleware: new MiddlewareArray().concat(sagaMiddleware, routerMiddleware(history)),
    preloadedState,
  });

  sagaMiddleware.run(rootSaga);

  return { store, history };
};

export const { store, history } = configureAppStore({});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
