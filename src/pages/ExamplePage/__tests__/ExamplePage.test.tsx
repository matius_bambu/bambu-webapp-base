import userEvent from '@testing-library/user-event';

import { render, screen } from 'utils/testUtils';

import { ExamplePage } from '..';

describe('ExamplePage', () => {
  describe('successful submission', () => {
    test('display message if user is successfully logged in', async () => {
      render(<ExamplePage />);

      // type username
      const userNameField = screen.getByLabelText(/username/i);
      userEvent.type(userNameField, 'matius@bambu.co');

      // type password
      const passwordField = screen.getByLabelText(/password/i);
      userEvent.type(passwordField, 'Bambu@01');

      // submit form
      const submitBtn = await screen.findByRole('button', { name: /submit/i });
      userEvent.click(submitBtn);

      // message is displayed
      const loginMessage = await screen.findByText(/you are logged in/i);
      expect(loginMessage).toBeInTheDocument();
    });
  });

  describe('errors', () => {
    test('display invalid email error if userName is not an email', async () => {
      render(<ExamplePage />);

      // type username
      const userNameField = screen.getByLabelText(/username/i);
      userEvent.type(userNameField, 'matius');

      const invalidEmailMessage = await screen.findByText(/must be a valid email/i);
      expect(invalidEmailMessage).toBeInTheDocument();
    });
  });
});
