import Grid from '@material-ui/core/Grid';
import { useFormContext } from 'react-hook-form';

import { FieldWrapper, TextField } from 'components/Form';

import type { ExamplePageFormValuesProps } from './ExamplePageForm';

export const ExamplePageFormFields = (): JSX.Element => {
  const {
    control,
    formState: { errors },
  } = useFormContext<ExamplePageFormValuesProps>();

  return (
    <Grid spacing={1} container>
      <Grid item xs={12}>
        <FieldWrapper errorMessage={errors.userName?.message}>
          <TextField
            TextFieldProps={{
              id: 'username-field',
              label: 'Username',
              fullWidth: true,
            }}
            control={control}
            name="userName"
          />
        </FieldWrapper>
      </Grid>
      <Grid item xs={12}>
        <FieldWrapper errorMessage={errors.password?.message}>
          <TextField
            TextFieldProps={{
              id: 'password-field',
              label: 'Password',
              fullWidth: true,
              type: 'password',
            }}
            control={control}
            name="password"
          />
        </FieldWrapper>
      </Grid>
    </Grid>
  );
};
