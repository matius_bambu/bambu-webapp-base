import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';

import { Helmet } from 'components/Helmet';
import { ExamplePageForm } from './ExamplePageForm';

export const ExamplePage = (): JSX.Element => (
  <>
    <Helmet title="Example" />
    <Box paddingTop={3} paddingBottom={3}>
      <Grid spacing={3} container>
        <Grid item xs={12}>
          <Typography variant="h4" align="center">
            Example Page
          </Typography>
        </Grid>
        <Grid item xs={12}>
          <Box display="flex" justifyContent="center">
            <ExamplePageForm />
          </Box>
        </Grid>
      </Grid>
    </Box>
  </>
);
