import { yupResolver } from '@hookform/resolvers/yup';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { useForm, FormProvider, SubmitHandler } from 'react-hook-form';
import * as Yup from 'yup';

import { useDispatch, useSelector } from 'hooks';
import { submitCredentialRequest } from './actions';
import { ExamplePageFormFields } from './ExamplePageFormFields';
import { selectExamplePageIsCredentialAuthenticated } from './selectors';

const examplePageFormSchema = Yup.object().shape({
  userName: Yup.string().email().required(),
  password: Yup.string().required(),
});

export type ExamplePageFormValuesProps = {
  userName: string;
  password: string;
};

export const ExamplePageForm = (): JSX.Element => {
  const methods = useForm<ExamplePageFormValuesProps>({
    defaultValues: {
      userName: '',
      password: '',
    },
    mode: 'onChange',
    resolver: yupResolver(examplePageFormSchema),
  });
  const isBnDisabled = !methods.formState.isValid;

  const dispatch = useDispatch();
  const isCredentialAuthenticated = useSelector(selectExamplePageIsCredentialAuthenticated);

  const onSubmit: SubmitHandler<ExamplePageFormValuesProps> = (values) => {
    dispatch(submitCredentialRequest(values));
  };

  return (
    <FormProvider {...methods}>
      <form
        onSubmit={methods.handleSubmit(onSubmit)}
        id="example-page-form"
        autoCapitalize="off"
        autoComplete="off"
      >
        <Grid spacing={3} container>
          <Grid item xs={12}>
            <ExamplePageFormFields />
          </Grid>
          <Grid item xs={12}>
            <Button
              disabled={isBnDisabled}
              variant="contained"
              color="primary"
              fullWidth
              type="submit"
            >
              Submit
            </Button>
          </Grid>
          {isCredentialAuthenticated ? (
            <Grid item xs={12}>
              <Typography>You are logged in!</Typography>
            </Grid>
          ) : null}
        </Grid>
      </form>
    </FormProvider>
  );
};
