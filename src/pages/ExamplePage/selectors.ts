import { createSelector } from '@reduxjs/toolkit';

import type { RootState } from 'configureStore';

const examplePageSelector = (state: RootState) => state.examplePage;

export const selectExamplePageIsCredentialAuthenticated = createSelector(
  examplePageSelector,
  (examplePage) => examplePage.isCredentialAuthenticated,
);
