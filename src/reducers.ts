import { combineReducers } from '@reduxjs/toolkit';
import { connectRouter } from 'connected-react-router';
import { History } from 'history';

import { examplePageReducer } from 'pages/ExamplePage/reducer';

export const createReducer = (history: History) => {
  const appReducer = combineReducers({
    router: connectRouter(history),
    examplePage: examplePageReducer,
  });

  // const rootReducer = (state, action) => appReducer(state, action);

  return appReducer;
};
