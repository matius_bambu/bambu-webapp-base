import MuiAppBar from '@material-ui/core/AppBar';
import IconButton from '@material-ui/core/IconButton';
import Toolbar from '@material-ui/core/Toolbar';
import MenuIcon from '@material-ui/icons/Menu';

export const AppBar = (): JSX.Element => (
  <MuiAppBar position="static">
    <Toolbar>
      <IconButton>
        <MenuIcon />
      </IconButton>
    </Toolbar>
  </MuiAppBar>
);
