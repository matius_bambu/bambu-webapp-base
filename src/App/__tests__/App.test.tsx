import { render, screen } from 'utils/testUtils';

import App from '..';

describe('App', () => {
  test('renders landing page', () => {
    const options = {
      route: '/',
    };
    render(<App />, options);

    const examplePage = screen.getByText(/example/i);
    expect(examplePage).toBeInTheDocument();
  });
});
