import Container from '@material-ui/core/Container';
import { makeStyles } from '@material-ui/core/styles';
import { Helmet } from 'react-helmet-async';
import { Switch, Route } from 'react-router-dom';

import { ExamplePage } from 'pages/ExamplePage';
import { AppBar } from './AppBar';

const useStyles = makeStyles({
  containerRoot: {
    flexGrow: 1,
  },
});

function App(): JSX.Element {
  const classes = useStyles();

  return (
    <>
      <Helmet titleTemplate="%s - My App" />
      <AppBar />
      <Container classes={{ root: classes.containerRoot }} maxWidth="md">
        <Switch>
          <Route exact component={ExamplePage} path="/" />
        </Switch>
      </Container>
    </>
  );
}

export default App;
